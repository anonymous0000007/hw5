"""Compatible for Python 2.x.x and 3.x.x"""

#!/usr/bin/python

import math
from sys import version_info

def twoMaxes(L):

    """Problem 1: find row and column max of two dimesional list"""

    # final result
    result = []

    # row and column max
    rowMax = []
    colMax = []

    # finding row max
    for row in L:
        rowMax.append(max(row))

    # finding col max
    for index in range(len(L[0])):
        col = []
        for row in L:
            col.append(row[index])  
        colMax.append(max(col))

    # append rowMax and colMax in final result
    result.append(rowMax)
    result.append(colMax)

    # release rowMax and colMax variable memory
    del rowMax
    del colMax

    return result

def dictionaryCollector(L):

    """Problem 2: collect integer and string in dictionary"""

    integer = 0
    string = ""

    for el in L:

        # isinteger checking condition
        if type(el) == type(0):
            integer += el
        # isstring checking condition
        elif type(el) == type(""):
            string += el

    # bind and return dictionary as result
    return { "int" : integer, "string" : string }

def separateNumbers(L):

    """Problem 3: separate odd and even number using recursion"""

    # finalResult control variable
    finalResult = [[],[]]

    # remove element from given list
    el = L.pop(0)

    # check if element is odd or even
    if el % 2 == 0:
        finalResult[1].append(el)
    else:
        finalResult[0].append(el)

    if len(L) == 0:
        return finalResult

    # stored return result in tempResult
    tempResult = separateNumbers(L)
    
    # add tempResult on finalResult for return
    finalResult[0] += tempResult[0]
    finalResult[1] += tempResult[1]

    # release tempResult memory
    del tempResult

    return finalResult

# new style class for specially python 2 
class Circle(object):

    """Problem 4: Circle and its world"""

    # constructor
    def __init__(self,radius):
        self.radius = radius

    # call when object print
    def __str__(self):
        return "Radius: %d"%(self.radius)

    # calculate area of circle
    def area(self):
        return math.pi * self.radius * self.radius

class Sphere(Circle):

    """Problem 4: Sphere and its world"""

    # constructor
    def __init__(self,radius):
        super(Sphere,self).__init__(radius)
        self.radius = radius

    # call when object print
    def __str__(self):
        return "Radius: %d"%self.radius

    # calculate area of sphere
    def area(self):
        return round(4 * super(Sphere,self).area(), 2)

    # calculate volume of sphere
    def volume(self):
        # covert 3 into float for compatiblity issue(Python 2.x.x and 3.x.x)
        return round((4/float(3)) * super(Sphere,self).area() * self.radius, 2)

def preciseDivision(a,b):

    "Problem 5: return precise answer of division of two number"

    try:
        if version_info.major == 2:
            return a/float(b)
        else:
            return a/b
    except ZeroDivisionError:
        # return infinity if b equal to 0
        # checking python v2.x.x or v3.x.x
        if version_info.major == 2:
            return float("inf")
        else:
            return float(math.inf)
    except Exception:
        return None


# condition to avoid function call and its result for library purpose
if __name__ == "__main__":

    # Problem 1: two dimensional list and testing
    inputList = [[1,2],[3,4]]
    L = twoMaxes(inputList)
    print(L)

    # Problem 2: testing
    L = [True, 1, 4, 5, "hello", 10, "10", "world"]
    d = dictionaryCollector(L)
    print(d)

    # Problem 3: testing
    inputList = [1,2,3,4,5,6,7,8,9,10]
    L = separateNumbers(inputList)
    print(L)

    # Problment 4: testing
    c = Circle(3)
    s = Sphere(4)
    print(c)
    print(s)
    print(s.area())
    print(s.area())
    print(s.volume())

    # Problem 5: testing
    print(preciseDivision(1,2))
    print(preciseDivision(1,0))
    print(preciseDivision("Hello",True))