# Name
hw5

## Authors

- [@anonymous0000007 - github](https://github.com/anonymous0000007)
- [@anonymous0000007 - gitlab](https://gitlab.com/anonymous0000007)

# Version
v1.0.0

# Dependency
Python 2.x.x or 3.x.x

# Description
CS-515-Homework-5.pdf 5 questions solutions(recursion and inheritance etc.)

# Documentation

#### Problem 1 :
````
    twoMaxes() method
````

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `L` | `list` | **Required**. numbers list |

| Return   | Description                |
| :------- | :------------------------- |
| `list<list>` | return rowMax and colMax list |

#### Problem 2 :
````
    dictionaryCollector() method
````

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `L` | `list` | **Required**. numbers list |

| Return Type     | Description                |
| :------- | :------------------------- |
| `dictionary` | return rowMax and colMax list |

#### Problem 3 :
````
    separateNumbers() method
````

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `L` | `list` | **Required**. numbers list |

| Return Type     | Description                |
| :------- | :------------------------- |
| `list<list>` | return odd and even number list of list |

#### Problem 4 :
````
    Circle class
````
| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `radius` | `int` | **Required**. radius of circle |

````
    Sphere class
````
| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `radius` | `int` | **Required**. radius of sphere |

#### Problem 5 :
````
    preciseDivision() method
````
| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `a` | `int` | **Required**. dividend |
| `b` | `int` | **Required**. divisor |

| Return Type     | Description                |
| :------- | :------------------------- |
| `float` | division of a/b successfully |
| `infinity` | if b equal 0 then return inf |
| `None` | any other error occured |